## Wallpapers
### Synopsis
This repository exists to allow me to version control my wallpapers and easily synchronize them across
multiple devices. I do not hold the copyrights to these images (which is why there is no LICENSE file)
though most should be freely redistributable. If you have a copyright complaint, create an issue and I'll
take the image down.

The files are renamed based the SHA224 hash of their contents to remove duplicates. I use [darch](https://gitlab.com/ammongit/darch), a program I wrote, to accomplish this.
I use the `-m` option to avoid creating an archive but simply build the file tree and rename files. After the initial first build new of modified files will be hashed very quickly.

I try to organize my wallpapers in loose groups. See the "Organization" for more details.

Also, if you're watching a show and you see a directory for it, there may be spoilers. You have been warned.

I can't imagine why you would want to, but if you'd like you can submit wallpapers via merge requests. No
NSFW or NSFL wallpapers please.

### Organization
All wallpapers are located in `desktop`. This is because `feh`, the program I use to randomly choose a wallpaper,
will look inside the `.git` folder and sometimes choose a wallpaper in there. The simplest solution was
to move it all into a directory one deeper. Later I added the `phone` directory, which stores my phone wallpapers. These are not organized at all.

Here are explanations for each category:
#### anime
Self-explanatory, basically a container directory to store wallpapers pertaining to a certain show or
game or something.

#### drawn
Any wallpapers that are animated, drawn or sketched that don't have a more specific category to belong to.

#### fantasy
Any wallpapers that depict some sort of fantasy world. Any scene you could imagine seeing in a parallel world
belongs here, but note that "futuristic" wallpapers should go in the `scifi` folder. Wallpapers about the past
(e.g. a medieval tavern) belong here as well.

#### games
Any wallpapers pertaining to video games. Like `anime`, this category has sub-directories for specific
games.

#### general
The catch-all category for wallpapers. If you don't know where it goes, it probably should live here.

#### interesting
I wanted to call this category "humor", but not everything in here is funny. Just look inside to get
an idea as to the sort of content I'm aiming for here.

#### interior
Any wallpapers that feature an scene indoors.

#### landscape
This category is basically the same as nature, but with one important difference: there is something
man-made in the wallpaper.

#### linux
Category for any wallpapers about any operating system or pictures of Lord and Savior [rms](https://stallman.org).
The subcategory `arch` is for Arch Linux wallpapers specifically.

#### misc
This is similar to `general`, but for wallpapers that are too, erm, different? unique? I'd like to keep
this category smaller, and have its contents be more weird than they are now.

#### nature
For pictures of the natural world. Absolutely no evidence of human activity may exist in the picture.
If you have a nice scene of a hill, but there are some houses on it, the picture belongs in `landscape`.
While technically part of nature, pictures of the universe or the Earth belong in `space`.

#### poly
For wallpapers that utilize visible polygons as an artistic technique.

#### scifi
"Futuristic" or otherwise science fiction-related wallpapers belong here.

#### simple
Wallpapers with a simple, minimal design.

#### space
Pictures, real or not, of the universe in some way. Planets, galaxies, asteriods, whatever. Evidence
of human activity is ok as long as its real. If its fictional, it belongs in `scifi`.

#### tech
Pictures of technology, like fancy military planes and server farms.

#### urban
Scenes inside cities or other places with buildings. Fictional cities belong in either `drawn` or `scifi`.

